<#
.SYNOPSIS
    MasterPromptPS is a Light Touch build menu generator for Microsoft Endpoint Configuration Manager.
.DESCRIPTION
    MasterPromptPS is a Light Touch build menu generator for Microsoft Endpoint Configuration Manager. Values selected in the form are stored as task sequence variables.
.PARAMETER SettingsFile
    Specifies the json file with the settings and menu definition.
.PARAMETER HideProgress
    Hide the SCCM/MDT task sequence progress bar.
.PARAMETER SetVariables
    Generate SCCM/MDT Task Sequence variables
.PARAMETER Test
    Run in test mode.
.LINK
    https://gitlab.com/1st-technologies/powershell/masterpromptps
.INPUTS
    None. MasterPromptPS does not accept object inputs.
.OUTPUTS
    None, MasterPromptPS does not output any objects.
.EXAMPLE
    C:\ PS> .\MasterPromptPS -SetVariables
    Display a prompt as defined by the MasterPromptPS.json file (default) located in the current directory and store results in answers.json and as task sequence variables.
.EXAMPLE
    C:\ PS> .\MasterPromptPS -HideProgress -SetVariables
    Hide the MDT or SCCM progress bar, display a prompt as defined by the MasterPromptPS.json file (default) located in the current directory and store results in answers.json and as task sequence variables.
.EXAMPLE
    C:\ PS> .\MasterPromptPS -SettingsFile .\server.json
    Display a prompt as defined by .\server.json and store results in answers.json.
.NOTES
    Name     : MasterPromptPS
    Version  : 0.1.6
    Author   : 1st Technologies, Inc.
    Date     : October 2, 2015
    Copyright: (c) 2022 1st Technologies.
    Changes  : 0.1.0 - Initial Release
               0.1.1 - Add custom validation
               0.1.2 - Add cascade field and data file version validation
                       Add footer field
                       Remove Cancel button, unbind the "Escape" key from cancel and remove control box "X" button
                       Added headerColor settings
               0.1.3 - Add support for multiple radio button groups, misc. improvements
               0.1.4 - Minor updated to prep for json validation
               0.1.5 - Add WMI query support to the select field type; add custom actions; store answers in file
               0.1.6 - Add sort to custom actions call; add hostname field and hostname custom validation; add PTR record check custom action; misc formatting
#>


[CmdletBinding()]
Param(
    [Parameter(Mandatory = $false, HelpMessage="Specifies the settings file.")]
    [ValidateScript({
        If(Test-Path -Path $_ -PathType Leaf){
            $true
        }else{
            Throw "Settings file not found: $_"
        }
    })]
    [ValidateNotNullOrEmpty()]
    [string]$SettingsFile = "$PSScriptRoot\MasterPromptPS.json",

    [Parameter(Mandatory = $false, HelpMessage="Hide the SCCM/MDT progress bar.")]
    [switch]$HideProgress,

    [Parameter(Mandatory = $false, HelpMessage="Generate SCCM/MDT Task Sequence variables.")]
    [switch]$SetVariables,

    [Parameter(Mandatory = $false, HelpMessage="Run in test mode.")]
    [switch]$Test
)


Begin{
    $Version = "0.1.6"
    $answers = @{}

    Write-Verbose -Message "Defining Functions..."
    Function WMIQuery{
        Param(
            [Parameter(Mandatory = $true, HelpMessage="A valid WQL query.")]
            [ValidateNotNullOrEmpty()]
            [string]$query,

            [Parameter(Mandatory = $false, HelpMessage="Specifies the WMI namespace to query. Default [root\CimV2]")]
            [ValidateNotNullOrEmpty()]
            [string]$namespace="root\CimV2"
        )

        $class = Get-WMIObject -Query $query -Namespace $namespace

        If($class.Properties.IsArray){
            return $class.$($class.properties.name)[0]
        }else{
            return $class.$($class.properties.name)
        }
    }

    Function SetTSVariable{
        Param(
            [Parameter(Mandatory = $True, HelpMessage = "Specify the variable name")]
            [ValidateNotNullOrEmpty()]
            [string]$Name,
            [Parameter(Mandatory = $True, HelpMessage = "Specify the variable value")]
            [ValidateNotNullOrEmpty()]
            [string]$Value
        )

        $smsTs = New-Object -ComObject Microsoft.SMS.TSEnvironment
        $smsTs[$Name] = $Value
        [System.InteropServices.Marshal]::ReleaseComObject($smsTs) | Out-Null
    }

    Write-Verbose -Message "Reading Settings file..."
    $Settings = Get-Content -Path $SettingsFile | ConvertFrom-Json
    If($Settings.__version__ -ne $Version){
        Throw "Invalid Settings file version $($Settings.__version__)."
    }

    Write-Verbose -Message "Loading Assemblies..."
    Try{
        [void][System.Reflection.Assembly]::LoadWithPartialName("System.Drawing")
        [void][System.Reflection.Assembly]::LoadWithPartialName("System.Windows.Forms")
    }Catch{
        Throw("Error loading assembly: {0}" -f $($_.exception.message)).ToString().Trim()
    }

    Function PopulateCascade($mCB, $mDCB, $mCascade){
        $mCB.items.Clear()
        $cascadeItems = $($($Settings.fields | Where-Object{$_.name -eq $mCascade}).options.$($mDCB.SelectedItem.Value)).psObject.properties

        $objects = @()
        Foreach ($selectOption in $cascadeItems){
            $obj = new-object -TypeName System.Object
            $obj | add-member -type NoteProperty -name Label -value $selectOption.value
            $obj | add-member -type NoteProperty -name Value -value $selectOption.name
            $objects += $obj
        }

        $mCB.items.addRange($objects)
        $mCB.DisplayMember = "label"
        $mCB.ValueMember = "Value"

        $mCB.Items.Insert(0, "Select an option")
        $mCB.SelectedIndex = 0

        $mCB.Enabled = $True
    }
}

Process{
    If($HideProgress){
        $smsUi = New-Object -ComObject "Microsoft.SMS.TSProgressUI"
        $smsUi.CloseProgressDialog()
        [System.InteropServices.Marshal]::ReleaseComObject($smsUi) | Out-Null
    }

    Write-Verbose -Message "Drawing form..."
    $objForm = New-Object -TypeName System.Windows.Forms.Form
    $objForm.Text = $Settings.title
    $objForm.Size = [system.drawing.size]::new($Settings.width, $Settings.height)
    $objForm.StartPosition = "CenterScreen"
    $objIcon = [System.Drawing.Icon]::new("$PSScriptRoot\images\favicon.ico")
    $objForm.Icon = $objIcon
    $objForm.BackgroundImage = $objImage
    $objForm.BackgroundImageLayout = "None"
    $objForm.FormBorderStyle = [System.Windows.Forms.FormBorderStyle]::FixedDialog
    $objForm.MaximizeBox = $false
    $objForm.MinimizeBox = $false
    $objForm.ControlBox = $false

    Write-Verbose -Message "Defining fonts..."
    $LabelFont = [System.Drawing.Font]::new("Arial",10,[System.Drawing.FontStyle]::Bold)
    $FieldFont = [System.Drawing.Font]::new("Arial",10,[System.Drawing.FontStyle]::Regular)
    $HeaderFont = [System.Drawing.Font]::new("Arial",18,[System.Drawing.FontStyle]::Bold)

    Write-Verbose -Message "Wiring keyboard hooks..."
    $objForm.KeyPreview = $True
    $objForm.Add_KeyDown({
        if($_.KeyCode -eq "Enter"){
            $OKButton.PerformClick()
        }
    })

    If($Test){
        $objForm.Add_KeyDown({
            if($_.KeyCode -eq "Escape"){
                $objForm.Close()
            }
        })
    }

    $objImage = [system.drawing.image]::FromFile("$PSScriptRoot\images\logo.png")
    $oPictureBox = New-Object -TypeName Windows.Forms.PictureBox
    $oPictureBox.Image = $objImage
    #$oPictureBox.Width =
    $oPictureBox.AutoSize = $true
    $oPictureBox.Location = [system.drawing.size]::new(8,17)

    $objHeaderPanel = New-Object -TypeName System.Windows.Forms.Panel
    $objHeaderPanel.BackColor = [System.Drawing.ColorTranslator]::FromHtml($Settings.headerColor)
    $objHeaderPanel.Dock = [System.Windows.Forms.DockStyle]::Top
    $objHeaderPanel.SendToBack()

    $objHeaderLabel = New-Object -TypeName System.Windows.Forms.Label
    $objHeaderLabel.Location = [system.drawing.size]::new(10,5)
    $objHeaderLabel.TextAlign = [System.Drawing.ContentAlignment]::MiddleCenter
    $objHeaderLabel.Size = [System.Drawing.Size]::new($Settings.width, 20)
    $objHeaderLabel.Text = $Settings.header
    $objHeaderLabel.Font = $HeaderFont
    $objHeaderLabel.BackColor = "Transparent"
    $objHeaderLabel.ForeColor = "White"
    $objHeaderPanel.Controls.Add($oPictureBox)
    $objHeaderPanel.Controls.Add($objHeaderLabel)
    $objForm.Controls.Add($objHeaderPanel)

    ForEach ($Field in $Settings.fields){
        Write-Verbose -Message "Generating $($Field.type) field..."
        Switch ($Field.type){
            {($_ -eq "info") -or ($_ -eq "text") -or ($_ -eq "password") -or ($_ -eq "radio") -or ($_ -eq "select") -or ($_ -eq "check") -or ($_ -eq "cascade")} {
                $Settings.rowY += $Settings.rowHeight
                Set-Variable -Name "l$($Field.name)" -Value $(New-Object -TypeName System.Windows.Forms.Label)
                Invoke-Expression('$l' + $Field.name + '.Text = "' + $Field.label + '"')
                Invoke-Expression('$l' + $Field.name + '.AutoSize = $true')
                Invoke-Expression('$l' + $Field.name + '.Location = [system.drawing.size]::new(' + $Settings.labelColumnX + ', ' + $Settings.rowY + ')')
                Invoke-Expression('$l' + $Field.name + '.Font = $LabelFont')
                Invoke-Expression('$objForm.Controls.Add($l' + $Field.name + ')')
            }

            "info"{
                Set-Variable -Name "f$($Field.name)" -Value $(New-Object -TypeName System.Windows.Forms.Label)
                Invoke-Expression('$f' + $Field.name + '.Text = WMIQuery(' + [char]34 + $Field.query + [char]34 +')')
                Invoke-Expression('$f' + $Field.name + '.AutoSize = $true')
                Invoke-Expression('$objForm.Controls.Add($f' + $Field.name + ')')
            }

            "text"{
                Set-Variable -Name "f$($Field.name)" -Value $(New-Object -TypeName System.Windows.Forms.TextBox)
                Invoke-Expression('$f' + $Field.name + '.MaxLength = ' + $Field.maxLength)
                If($Field.default){
                    Invoke-Expression('$f' + $Field.name + '.Text = WMIQuery(' + [char]34 + $Field.default + [char]34 +')')
                }
                Invoke-Expression('$objForm.Controls.Add($f' + $Field.name + ')')
            }

            "password"{
                Set-Variable -Name "f$($Field.name)" -Value $(New-Object -TypeName System.Windows.Forms.TextBox)
                Invoke-Expression('$f' + $Field.name + '.PasswordChar = "*"')
                Invoke-Expression('$objForm.Controls.Add($f' + $Field.name + ')')
            }

            "radio"{
                Set-Variable -Name "f$($Field.name)" -Value $(New-Object -TypeName System.Windows.Forms.GroupBox)
                Invoke-Expression('$f' + $Field.name + '.Size = [system.drawing.size]::new(' + $Settings.textBoxSize + ', ' + ($Settings.rowHeight * 2) + ')')
                Invoke-Expression('$f' + $Field.name + '.Location = [system.drawing.size]::new(' + $Settings.fieldColumnX + ', ' + ($Settings.rowY - 2) + ')')

                Foreach ($radioOption in $Field.options.psObject.properties){
                    Write-Verbose -Message "Generating Radio Button Option: $($radioOption.name)"
                    Set-Variable -Name "f$($Field.name)_$($radioOption.name)" -Value $(New-Object -TypeName System.Windows.Forms.RadioButton)
                    Invoke-Expression('$f' + $Field.name + '_' + $radioOption.name + '.dock = [System.Windows.Forms.DockStyle]::Left')
                    Invoke-Expression('$f' + $Field.name + '_' + $radioOption.name + '.Text = "' + $radioOption.value + '"')
                    Invoke-Expression('$f' + $Field.name + '_' + $radioOption.name + '.Font = $FieldFont')
                    Invoke-Expression('$f' + $Field.name + '.Controls.Add($f' + $Field.name + "_" + $radioOption.name + ')')
                    If($Field.default -eq $radioOption.name){
                        Invoke-Expression('$f' + $Field.name + '_' + $radioOption.name + '.Checked = $True')
                    }
                }
                $Settings.rowY += ($Settings.rowHeight * 1.3)
                Invoke-Expression('$objForm.Controls.Add($f' + $Field.name + ')')
                Write-verbose -Message "RADIO: $($Field.Name)"
                Write-verbose -Message "RADIO: $($fRadioEx.Controls)"
            }

            "check"{
                Set-Variable -Name "f$($Field.name)" -Value $(New-Object -TypeName System.Windows.Forms.CheckBox)
                Invoke-Expression('$f' + $Field.name + '.AutoSize = $true')
                Invoke-Expression('$f' + $Field.name + '.Location = [system.drawing.size]::new(' + $Settings.fieldColumnX + ', ' + ($Settings.rowY + 4) + ')')
                Invoke-Expression('$objForm.Controls.Add($f' + $Field.name + ')')
                If($Field.checked){
                    Invoke-Expression('$f' + $Field.name + '.Checked = $True')
                }
            }

            "select"{
                Set-Variable -Name "f$($Field.name)" -Value $(New-Object -TypeName System.Windows.Forms.ComboBox)
                Invoke-Expression('$f' + $Field.name + '.DropDownStyle = [System.Windows.Forms.ComboBoxStyle]::DropDownList')
                $objects = @()

                If($Field.options){
                    Foreach ($selectOption in $Field.options.psObject.properties){
                        $obj = new-object -TypeName System.Object
                        $obj | add-member -type NoteProperty -name Label -value $selectOption.value
                        $obj | add-member -type NoteProperty -name Value -value $selectOption.name
                        $objects += $obj
                    }
                }

                If($Field.default){
                    $wmiResults = Get-CimInstance -Query $Field.default
                    ForEach ($wmiResult In $wmiResults){
                        $obj = new-object -TypeName System.Object
                        $obj | add-member -type NoteProperty -name Label -value $wmiResult.$($Field.wmiLabel)
                        $obj | add-member -type NoteProperty -name Value -value $wmiResult.$($Field.wmiValue)
                        $objects += $obj
                    }
                }

                Invoke-Expression('$f' + $Field.name + '.items.addRange($objects)')
                Invoke-Expression('$f' + $Field.name + '.DisplayMember = "Label"')
                Invoke-Expression('$f' + $Field.name + '.ValueMember = "Value"')

                Invoke-Expression('$f' + $Field.name + '.Items.Insert(0, "Select an option")')
                Invoke-Expression('$f' + $Field.name + '.SelectedIndex = 0')
                Invoke-Expression('$objForm.Controls.Add($f' + $Field.name + ')')
            }

            "cascade"{
                Invoke-Expression('$f' + $Field.name + ' = New-Object -TypeName System.Windows.Forms.ComboBox')
                Invoke-Expression('$f' + $Field.name + '.DropDownStyle = [System.Windows.Forms.ComboBoxStyle]::DropDownList')
                Invoke-Expression('$f' + $Field.name + '.Enabled = $False')

                Invoke-Expression('$f' + $Field.dependsOn + '.add_SelectedIndexChanged{PopulateCascade $f' + $Field.name + ' $f' + $Field.dependsOn + ' ' + $Field.name + '}')

                Invoke-Expression('$f' + $Field.name + '.Items.Insert(0, "Select an option")')
                Invoke-Expression('$f' + $Field.name + '.SelectedIndex = 0')
                Invoke-Expression('$objForm.Controls.Add($f' + $Field.name + ')')
            }

            {($_ -eq "text") -or ($_ -eq "password") -or ($_ -eq "select") -or ($_ -eq "cascade")} {
                Invoke-Expression('$f' + $Field.name + '.Size = [system.drawing.size]::new(' + $Settings.textBoxSize + ', 20)')
            }

            {($_ -eq "info") -or ($_ -eq "text") -or ($_ -eq "password") -or ($_ -eq "select") -or ($_ -eq "cascade")} {
                Invoke-Expression('$f' + $Field.name + '.Font = $FieldFont')
                Invoke-Expression('$f' + $Field.name + '.Location = [system.drawing.size]::new(' + $Settings.fieldColumnX + ', ' + $Settings.rowY + ')')
            }

            Default{throw "Unknown prompt rule $($Field.type) in $SettingsFile"}
        }
    }

    $objForm.Controls | ForEach-Object{
        Write-Verbose -Message "=== $_ ==="
    }

    $Settings.rowY += $Settings.rowHeight + 20

    #$chkDebug = New-Object -TypeName System.Windows.Forms.CheckBox
    #$chkDebug.AutoSize = $true
    #$chkDebug.BringToFront()
    #$chkDebug.Location = [System.Drawing.Size]::new($settings.width - 35, 105)
    #$chkDebug.BringToFront()
    #$objForm.Controls.Add($chkDebug)

    $okButtonX = ($objForm.Width/2)-55
    $OKButton = New-Object -TypeName System.Windows.Forms.Button
    $OKButton.Location = [system.drawing.size]::new($okButtonX, $Settings.rowY)
    $OKButton.Size =  [system.drawing.size]::new(100, 25)
    $OKButton.Text = "OK"
    $OKButton.Add_Click{
        $valid = $true
        $cValid = $true
        ForEach ($Field in $Settings.fields){
            Switch ($Field.type){
                {($_ -eq "text") -or ($_ -eq "password")}{
                    If($Field.required -eq $true){
                        Invoke-Expression('if ($f' + $Field.name + '.text.length -eq 0){$valid = $false; $f' + $Field.name + '.BackColor = [System.Drawing.ColorTranslator]::FromHtml("' + $Settings.errorColor + '")}else{$f' + $Field.name + '.ResetBackColor()}')
                        If(Test-Path -Path "$PSScriptRoot\customvalidators\$($Field.name).ps1"){
                            $cValid=(. "$PSScriptRoot\customvalidators\$($Field.name).ps1" -Value (Invoke-Expression('$f' + $Field.name + '.text')))
                        }
                    }
                }

                {($_ -eq "select") -or ($_ -eq "cascade")}{
                    Invoke-Expression('if ($f' + $Field.name + '.SelectedIndex -eq 0){$valid = $false; $f' + $Field.name + '.BackColor = [System.Drawing.ColorTranslator]::FromHtml("' + $Settings.errorColor + '")}else{$f' + $Field.name + '.ResetBackColor()}')
                }
            }
        }
        if(($valid -eq $true) -and ($cValid -eq $true)){
            Write-Verbose -Message "Get field values"
            ForEach ($Field in $Settings.fields){
                Switch ($Field.type){
                    "text"{
                        $fVal = Invoke-Expression('$f' + $Field.name + '.Text')
                        If($SCCM){SetTSVariable -Name $Field.name -Value $fVal}
                        $answers.Add($Field.name, $fVal)
                    }

                    "password"{
                        $fVal = Invoke-Expression('$f' + $Field.name + '.Text') | ConvertTo-SecureString -AsPlainText -Force | ConvertFrom-SecureString
                        If($SCCM){SetTSVariable -Name $Field.name -Value $fVal}
                        $answers.Add($Field.name, $fVal)
                    }

                    "radio"{
                        Foreach ($radioOption in $Field.options.psObject.properties){
                            Invoke-Expression('if($f' + $Field.name + '_' + $radioOption.name + '.checked -eq $true){$fVal = $f' + $Field.name + '_' + $radioOption.name + '}')
                        }
                        $fLabel = $Field.options.psObject.Properties | Where-Object {$_.Value -eq $($fVal.Text)} | Select-Object -ExpandProperty Name
                        If($SCCM){SetTSVariable -Name $Field.name -Value $fVal.Text}
                        If($SCCM){SetTSVariable -Name "$($Field.Name)_label" -Value $fLabel}
                        $answers.Add($Field.name, $fVal.Text)
                        $answers.Add("$($Field.Name)_label", $fLabel)
                    }

                    "check"{
                        $fVal = Invoke-Expression('$f' + $Field.name + '.Checked')
                        If($SCCM){SetTSVariable -Name $Field.name -Value $fVal}
                        $answers.Add($Field.name, $fVal)
                    }

                    "select"{
                        $fVal = Invoke-Expression('$f' + $Field.name + '.SelectedItem')
                        If($SCCM){SetTSVariable -Name $Field.name -Value $fVal.Value}
                        If($SCCM){SetTSVariable -Name "$($Field.Name)_label" -Value $fVal.Label}
                        $answers.Add($Field.name, $fVal.Value)
                        $answers.Add("$($Field.Name)_label", $fVal.Label)
                    }

                    "cascade"{
                        $fVal = Invoke-Expression('$f' + $Field.name + '.SelectedItem')
                        If($SCCM){SetTSVariable -Name $Field.name -Value $fVal.Value}
                        If($SCCM){SetTSVariable -Name "$($Field.Name)_label" -Value $fVal.Label}
                        $answers.Add($Field.name, $fVal.Value)
                        $answers.Add("$($Field.Name)_label", $fVal.Label)
                    }
                }
            }

            $answers_json = $answers | ConvertTo-Json
            Write-Verbose -Message $answers_json
            Set-Content -Value $answers_json -Path "$PSScriptRoot\answers.json"
            If(!($Test)){
                Get-ChildItem -Path "$PSScriptRoot\customactions" -Filter *.ps1 | Sort-Object | ForEach-Object{. "$PSScriptRoot\customactions\$($_.Name)"}
            }
            $objForm.Close()
        }
    }

    $objForm.Controls.Add($OKButton)

    $footerField = New-Object -TypeName System.Windows.Forms.Label
    $footerField.Dock = [System.Windows.Forms.DockStyle]::Bottom
    $footerField.Text = $Settings.footer
    $footerField.Font = $LabelFont
    $footerField.TextAlign = [System.Drawing.ContentAlignment]::MiddleCenter
    $footerField.Height = 50
    $footerField.BackColor = [System.Drawing.ColorTranslator]::FromHtml($Settings.errorColor)
    $objForm.Controls.Add($footerField)

    Write-Verbose -Message "Show dialog"
    $objForm.Topmost = $True
    $objForm.Add_Shown({$objForm.Activate()})
    [void]$objForm.ShowDialog()
}


End{
    Write-Verbose -Message "Done"
}