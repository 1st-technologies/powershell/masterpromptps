# NOTE: This script must be written to operate within WinPE which does not include the NetTCPIP module.

# Get Answers
$answers = Get-Content -Path "$PSScriptRoot\..\answers.json" | ConvertFrom-Json


# Check for matching PTR record
If(!((Resolve-DnsName -Name $answers.ipAddress -Type PTR -DnsOnly).NameHost.Split('.')[0]) -eq $answers.hostname){
    # Display error message
    $smsUi = New-Object -ComObject Microsoft.SMS.TSProgressUI
    $smsUi.CloseProgressDialog()
    $smsUi.ShowMessage('The DNS Record does not match the supplied IP address.', 'Error', 16)
    #$smsUi.ShowErrorDialog("Company Name", "Windows Server", "Error", "The DNS Record does not match the supplied IP address.", [uint64]9714, [uint64]4294967295, 1, "DNS REcord Check"); 
    exit 9714
}
