# NOTE: This script must be written to operate within WinPE which does not include the NetTCPIP module.

# Get Answers
$answers = Get-Content -Path "$PSScriptRoot\..\answers.json" | ConvertFrom-Json

Write-Output "Configuring $($answers.adapter_label)..."

# Get the network adapter by index
$adapter = Get-CimInstance -ClassName "Win32_NetworkAdapterConfiguration" -Filter "Index = $($answers.adapter)"

# Set the IP address, subnet mask, and default gateway
$adapter | Invoke-CimMethod -MethodName "EnableStatic" -Arguments @{IPAddress=@($($answers.ipAddress)); SubnetMask=@($($answers.subnetMask))}
$adapter | Invoke-CimMethod -MethodName "SetGateways" -Arguments @{DefaultIPGateway=@($($answers.gateway))}

# Set the DNS servers
$adapter | Invoke-CimMethod -MethodName "SetDNSServerSearchOrder" -Arguments @{DNSServerSearchOrder=$answers.dnsServers -split ","}

# Set the domain
$adapter | Invoke-CimMethod -MethodName "SetDNSDomain" -Arguments @{DNSDomain=$($answers.dnsSuffix)}

# set the DNS suffix search order
#$DNSSuffixes = "domain1.com", "domain2.com", "ny.domain3.com"
#$adapter | Invoke-CimMethod -MethodName SetDNSSuffixSearchOrder -Arguments @{DNSDomainSuffixSearchOrder=$DNSSuffixes}
