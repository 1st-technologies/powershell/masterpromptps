{
    "$schema": "http://json-schema.org/draft-07/schema",
    "$id": "http://example.com/example.json",
    "type": "object",
    "title": "MasterPrompt Menu schema",
    "description": "MasterPrompt Menu schema for menu file validation.",
    "default": {},
    "examples": [
        {
            "__version__": "0.1.6",
            "__description__": "Sample settings file",
            "width": 500,
            "height": 555,
            "rowHeight": 25,
            "labelColumnX": 10,
            "fieldColumnX": 150,
            "rowY": 90,
            "textBoxSize": 320,
            "title": "Build Prompt",
            "header": "",
            "footer": "Warning: This system is about to be rebuilt resulting in the loss of all data. If you DO NOT wish to proceed, immediately power off and remove any thumb drives.",
            "errorColor": "#FFFF77",
            "headerColor": "#47154A",
            "fields": [
                {
                    "type": "info",
                    "name": "infIpAddress",
                    "label": "IP Address",
                    "query": "SELECT IPAddress FROM Win32_NetworkAdapterConfiguration WHERE IPEnabled = True AND DHCPEnabled = True"
                },
                {
                    "type": "info",
                    "name": "InfMacAddress",
                    "label": "MAC Address",
                    "query": "SELECT MACAddress FROM Win32_NetworkAdapterConfiguration WHERE IPEnabled = True AND DHCPEnabled = True"
                },
                {
                    "type": "text",
                    "name": "OSDComputerName",
                    "label": "Computer Name",
                    "default": "SELECT SerialNumber FROM Win32_BIOS",
                    "required": true,
                    "maxLength": 15
                },
                {
                    "type": "select",
                    "name": "BUILD",
                    "label": "Build",
                    "options": {
                        "Standard": "Standard"
                    }
                },
                {
                    "type": "select",
                    "name": "adapter",
                    "label": "Adapter",
                    "default": "select Name,Index from Win32_NetworkAdapter where NetConnectionStatus = 2 AND AdapterTypeID = 0",
                    "wmiLabel": "Name",
                    "wmiValue": "Index"
                },
                {
                    "type": "password",
                    "name": "PASSWORD",
                    "label": "Password",
                    "required": true
                },
                {
                    "type": "check",
                    "name": "OPTION1",
                    "label": "Option 1?",
                    "checked": true
                },
                {
                    "type": "radio",
                    "name": "radioEx",
                    "label": "Radio Example",
                    "default": "standard",
                    "options": {
                        "Standard": "Standard"
                    }
                },
                {
                    "type": "radio",
                    "name": "radioEx2",
                    "label": "Radio Example 2",
                    "default": "rexO2",
                    "options": {
                        "rexO1": "Rex Opt 1"
                    }
                },
                {
                    "type": "cascade",
                    "name": "cascadeEx",
                    "label": "Cascade Example",
                    "dependsOn": "BUILD",
                    "options": {
                        "Standard": {
                            "std1": "Std 1"
                        }
                    }
                }
            ]
        }
    ],
    "required": [
        "__version__",
        "__description__",
        "width",
        "height",
        "rowHeight",
        "labelColumnX",
        "fieldColumnX",
        "rowY",
        "textBoxSize",
        "title",
        "header",
        "footer",
        "errorColor",
        "headerColor",
        "fields"
    ],
    "properties": {
        "__version__": {
            "$id": "#/properties/__version__",
            "type": "string",
            "title": "The __version__ schema",
            "description": "Set to the corresponding MasterPromptPS version.",
            "default": "",
            "examples": [
                "0.1.6"
            ]
        },
        "__description__": {
            "$id": "#/properties/__description__",
            "type": "string",
            "title": "The __description__ schema",
            "description": "An description of the purpose of the data file.",
            "default": "",
            "examples": [
                "Sample settings file"
            ]
        },
        "width": {
            "$id": "#/properties/width",
            "type": "integer",
            "title": "The width schema",
            "description": "The width of the dialog box.",
            "default": 640,
            "examples": [
                500
            ]
        },
        "height": {
            "$id": "#/properties/height",
            "type": "integer",
            "title": "The height schema",
            "description": "The height of the dialog box.",
            "default": 480,
            "examples": [
                555
            ]
        },
        "rowHeight": {
            "$id": "#/properties/rowHeight",
            "type": "integer",
            "title": "The rowHeight schema",
            "description": "The height of each field row.",
            "default": 25,
            "examples": [
                25
            ]
        },
        "labelColumnX": {
            "$id": "#/properties/labelColumnX",
            "type": "integer",
            "title": "The labelColumnX schema",
            "description": "The initial horizontal position of the label column in pixels.",
            "default": 0,
            "examples": [
                10
            ]
        },
        "fieldColumnX": {
            "$id": "#/properties/fieldColumnX",
            "type": "integer",
            "title": "The fieldColumnX schema",
            "description": "The initial horizontal position of the field column in pixels.",
            "default": 100,
            "examples": [
                150
            ]
        },
        "rowY": {
            "$id": "#/properties/rowY",
            "type": "integer",
            "title": "The rowY schema",
            "description": "The vertical position of the first field row in pixels.",
            "default": 0,
            "examples": [
                90
            ]
        },
        "textBoxSize": {
            "$id": "#/properties/textBoxSize",
            "type": "integer",
            "title": "The textBoxSize schema",
            "description": "The length of text boxes, password fields, dropdown lists and radio button groups in pixels.",
            "default": 0,
            "examples": [
                320
            ]
        },
        "title": {
            "$id": "#/properties/title",
            "type": "string",
            "title": "The title schema",
            "description": "The window title.",
            "default": "",
            "examples": [
                "Build Prompt"
            ]
        },
        "header": {
            "$id": "#/properties/header",
            "type": "string",
            "title": "The header schema",
            "description": "Text to appear beside the logo in the header of the form.",
            "default": "",
            "examples": [
                "Company Name"
            ]
        },
        "footer": {
            "$id": "#/properties/footer",
            "type": "string",
            "title": "The footer schema",
            "description": "Text to appear in the footer of the form.",
            "default": "",
            "examples": [
                "Warning: This system is about to be rebuilt resulting in the loss of all data. If you DO NOT wish to proceed, immediately power off and remove any thumb drives."
            ]
        },
        "errorColor": {
            "$id": "#/properties/errorColor",
            "type": "string",
            "title": "The errorColor schema",
            "description": "Set the background of text, password and select field if they do not validate.",
            "default": "",
            "examples": [
                "#FFFF77"
            ]
        },
        "headerColor": {
            "$id": "#/properties/headerColor",
            "type": "string",
            "title": "The headerColor schema",
            "description": "Set the background color of the header.",
            "default": "",
            "examples": [
                "#47154A"
            ]
        },
        "fields": {
            "$id": "#/properties/fields",
            "type": "array",
            "title": "The fields schema",
            "description": "An array of field definitions.",
            "default": [],
            "examples": [
                [
                    {
                        "type": "info",
                        "name": "infIpAddress",
                        "label": "IP Address",
                        "query": "SELECT IPAddress FROM Win32_NetworkAdapterConfiguration WHERE IPEnabled = True AND DHCPEnabled = True"
                    }
                ]
            ],
            "additionalItems": true,
            "items": {
                "$id": "#/properties/fields/items",
                "anyOf": [
                    {
                        "$id": "#/properties/fields/items/anyOf/0",
                        "type": "object",
                        "title": "The info field schema",
                        "description": "An information field that uses a WQL query as the data source.",
                        "default": {},
                        "examples": [
                            {
                                "type": "info",
                                "name": "infIpAddress",
                                "label": "IP Address",
                                "query": "SELECT IPAddress FROM Win32_NetworkAdapterConfiguration WHERE IPEnabled = True AND DHCPEnabled = True"
                            }
                        ],
                        "required": [
                            "type",
                            "name",
                            "label",
                            "query"
                        ],
                        "properties": {
                            "type": {
                                "$id": "#/properties/fields/items/anyOf/0/properties/type",
                                "type": "string",
                                "title": "The info type schema",
                                "description": "Defines the info field type.",
                                "default": "info",
                                "examples": [
                                    "info"
                                ]
                            },
                            "name": {
                                "$id": "#/properties/fields/items/anyOf/0/properties/name",
                                "type": "string",
                                "title": "The info name schema",
                                "description": "Defines the info variable name.",
                                "default": "",
                                "examples": [
                                    "infIpAddress"
                                ]
                            },
                            "label": {
                                "$id": "#/properties/fields/items/anyOf/0/properties/label",
                                "type": "string",
                                "title": "The info label schema",
                                "description": "Defines the info field label.",
                                "default": "",
                                "examples": [
                                    "IP Address"
                                ]
                            },
                            "query": {
                                "$id": "#/properties/fields/items/anyOf/0/properties/query",
                                "type": "string",
                                "title": "The info query schema",
                                "description": "Defines the info field WQL query.",
                                "default": "",
                                "examples": [
                                    "SELECT IPAddress FROM Win32_NetworkAdapterConfiguration WHERE IPEnabled = True AND DHCPEnabled = True"
                                ]
                            }
                        },
                        "additionalProperties": true
                    },
                    {
                        "$id": "#/properties/fields/items/anyOf/1",
                        "type": "object",
                        "title": "The text field schema",
                        "description": "A text field that can use a WQL query as the default value.",
                        "default": {},
                        "examples": [
                            {
                                "type": "text",
                                "name": "OSDComputerName",
                                "label": "Computer Name",
                                "default": "SELECT SerialNumber FROM Win32_BIOS",
                                "required": true,
                                "maxLength": 15
                            }
                        ],
                        "required": [
                            "type",
                            "name",
                            "label",
                            "required",
                            "maxLength"
                        ],
                        "properties": {
                            "type": {
                                "$id": "#/properties/fields/items/anyOf/1/properties/type",
                                "type": "string",
                                "title": "The text type schema",
                                "description": "Defines the text field type.",
                                "default": "",
                                "examples": [
                                    "text"
                                ]
                            },
                            "name": {
                                "$id": "#/properties/fields/items/anyOf/1/properties/name",
                                "type": "string",
                                "title": "The text name schema",
                                "description": "Defines the text field name.",
                                "default": "",
                                "examples": [
                                    "OSDComputerName"
                                ]
                            },
                            "label": {
                                "$id": "#/properties/fields/items/anyOf/1/properties/label",
                                "type": "string",
                                "title": "The text label schema",
                                "description": "Defines the text field label.",
                                "default": "",
                                "examples": [
                                    "Computer Name"
                                ]
                            },
                            "default": {
                                "$id": "#/properties/fields/items/anyOf/1/properties/default",
                                "type": "string",
                                "title": "The text default schema",
                                "description": "Specifies a WQL query that sets the text field default value.",
                                "default": "",
                                "examples": [
                                    "SELECT SerialNumber FROM Win32_BIOS"
                                ]
                            },
                            "required": {
                                "$id": "#/properties/fields/items/anyOf/1/properties/required",
                                "type": "boolean",
                                "title": "The text required schema",
                                "description": "Specifies if the field is required.",
                                "default": false,
                                "examples": [
                                    true
                                ]
                            },
                            "maxLength": {
                                "$id": "#/properties/fields/items/anyOf/1/properties/maxLength",
                                "type": "integer",
                                "title": "The text maxLength schema",
                                "description": "Specifies the maximum length of the text field.",
                                "default": 0,
                                "examples": [
                                    15
                                ]
                            }
                        },
                        "additionalProperties": true
                    },
                    {
                        "$id": "#/properties/fields/items/anyOf/2",
                        "type": "object",
                        "title": "The select field schema",
                        "description": "A combobox \"dropdown\" menu.",
                        "default": {},
                        "examples": [
                            {
                                "type": "select",
                                "name": "BUILD",
                                "label": "Build",
                                "options": {
                                    "standard": "Standard",
                                    "hr": "Human Resources"
                                }
                            }
                        ],
                        "required": [
                            "type",
                            "name",
                            "label"
                        ],
                        "properties": {
                            "type": {
                                "$id": "#/properties/fields/items/anyOf/2/properties/type",
                                "type": "string",
                                "title": "The select type schema",
                                "description": "The select field.",
                                "default": "select",
                                "examples": [
                                    "select"
                                ]
                            },
                            "name": {
                                "$id": "#/properties/fields/items/anyOf/2/properties/name",
                                "type": "string",
                                "title": "The select name schema",
                                "description": "The select field variable name.",
                                "default": "",
                                "examples": [
                                    "BUILD"
                                ]
                            },
                            "label": {
                                "$id": "#/properties/fields/items/anyOf/2/properties/label",
                                "type": "string",
                                "title": "The select label schema",
                                "description": "The select field label.",
                                "default": "",
                                "examples": [
                                    "Build"
                                ]
                            },
                            "default": {
                                "$id": "#/properties/fields/items/anyOf/2/properties/default",
                                "type": "string",
                                "title": "The select default schema",
                                "description": "Specifies a WQL query to define the menu items of the select menu.",
                                "default": "",
                                "examples": [
                                    "select Name,Index from Win32_NetworkAdapter where NetConnectionStatus = 2 AND AdapterTypeID = 0"
                                ]
                            },
                            "wmiLabel": {
                                "$id": "#/properties/fields/items/anyOf/2/properties/wmiLabel",
                                "type": "string",
                                "title": "The select wmiLabel schema",
                                "description": "Specifies the WMI property name to set the select menu item labels.",
                                "default": "",
                                "examples": [
                                    "Name"
                                ]
                            },
                            "wmiValue": {
                                "$id": "#/properties/fields/items/anyOf/2/properties/wmiValue",
                                "type": "string",
                                "title": "The select wmiValue schema",
                                "description": "Specifies the WMI property name to set the select menu item values.",
                                "default": "",
                                "examples": [
                                    "Index"
                                ]
                            },
                            "options": {
                                "$id": "#/properties/fields/items/anyOf/2/properties/options",
                                "type": "object",
                                "title": "The select options schema",
                                "description": "An array of objects defining the menu items.",
                                "default": {},
                                "examples": [
                                    {
                                        "standard": "Standard",
                                        "hr": "Human Resources"
                                    }
                                ],
                                "additionalProperties": true
                            }
                        },
                        "additionalProperties": true
                    },
                    {
                        "$id": "#/properties/fields/items/anyOf/3",
                        "type": "object",
                        "title": "The password field schema",
                        "description": "An explanation about the purpose of this instance.",
                        "default": {},
                        "examples": [
                            {
                                "type": "password",
                                "name": "PASSWORD",
                                "label": "Password",
                                "required": true
                            }
                        ],
                        "required": [
                            "type",
                            "name",
                            "label",
                            "required"
                        ],
                        "properties": {
                            "type": {
                                "$id": "#/properties/fields/items/anyOf/3/properties/type",
                                "type": "string",
                                "title": "The password type schema",
                                "description": "The password field type.",
                                "default": "password",
                                "examples": [
                                    "password"
                                ]
                            },
                            "name": {
                                "$id": "#/properties/fields/items/anyOf/3/properties/name",
                                "type": "string",
                                "title": "The password name schema",
                                "description": "The password field variable name.",
                                "default": "",
                                "examples": [
                                    "PASSWORD"
                                ]
                            },
                            "label": {
                                "$id": "#/properties/fields/items/anyOf/3/properties/label",
                                "type": "string",
                                "title": "The password label schema",
                                "description": "The password field label.",
                                "default": "",
                                "examples": [
                                    "Password"
                                ]
                            },
                            "required": {
                                "$id": "#/properties/fields/items/anyOf/3/properties/required",
                                "type": "boolean",
                                "title": "The password required schema",
                                "description": "Specifies if the field is required.",
                                "default": false,
                                "examples": [
                                    true
                                ]
                            }
                        },
                        "additionalProperties": true
                    },
                    {
                        "$id": "#/properties/fields/items/anyOf/4",
                        "type": "object",
                        "title": "The check field schema",
                        "description": "Defines a checkbox field type.",
                        "default": {},
                        "examples": [
                            {
                                "type": "check",
                                "name": "OPTION1",
                                "label": "Option 1?",
                                "checked": true
                            }
                        ],
                        "required": [
                            "type",
                            "name",
                            "label",
                            "checked"
                        ],
                        "properties": {
                            "type": {
                                "$id": "#/properties/fields/items/anyOf/4/properties/type",
                                "type": "string",
                                "title": "The checkbox type schema",
                                "description": "The checkbox field type.",
                                "default": "check",
                                "examples": [
                                    "check"
                                ]
                            },
                            "name": {
                                "$id": "#/properties/fields/items/anyOf/4/properties/name",
                                "type": "string",
                                "title": "The checkbox field name schema",
                                "description": "The checkbox field variable name.",
                                "default": "",
                                "examples": [
                                    "OPTION1"
                                ]
                            },
                            "label": {
                                "$id": "#/properties/fields/items/anyOf/4/properties/label",
                                "type": "string",
                                "title": "The checkbox field label schema",
                                "description": "The checkbox field label.",
                                "default": "",
                                "examples": [
                                    "Option 1?"
                                ]
                            },
                            "checked": {
                                "$id": "#/properties/fields/items/anyOf/4/properties/checked",
                                "type": "boolean",
                                "title": "The checkbox field checked schema",
                                "description": "The checkbox field default state.",
                                "default": false,
                                "examples": [
                                    true
                                ]
                            }
                        },
                        "additionalProperties": true
                    },
                    {
                        "$id": "#/properties/fields/items/anyOf/5",
                        "type": "object",
                        "title": "The radio field schema",
                        "description": "Defines a group of radio buttons.",
                        "default": {},
                        "examples": [
                            {
                                "type": "radio",
                                "name": "radioEx",
                                "label": "Radio Example",
                                "default": "standard",
                                "options": {
                                    "standard": "Standard",
                                    "hr": "Human Resources"
                                }
                            }
                        ],
                        "required": [
                            "type",
                            "name",
                            "label",
                            "default",
                            "options"
                        ],
                        "properties": {
                            "type": {
                                "$id": "#/properties/fields/items/anyOf/5/properties/type",
                                "type": "string",
                                "title": "The type schema",
                                "description": "The radio field type.",
                                "default": "radio",
                                "examples": [
                                    "radio"
                                ]
                            },
                            "name": {
                                "$id": "#/properties/fields/items/anyOf/5/properties/name",
                                "type": "string",
                                "title": "The radio field name schema",
                                "description": "Specifies the radio field variable name.",
                                "default": "",
                                "examples": [
                                    "radioEx"
                                ]
                            },
                            "label": {
                                "$id": "#/properties/fields/items/anyOf/5/properties/label",
                                "type": "string",
                                "title": "The radio field label schema",
                                "description": "Specifies the radio field variable label.",
                                "default": "",
                                "examples": [
                                    "Radio Example"
                                ]
                            },
                            "default": {
                                "$id": "#/properties/fields/items/anyOf/5/properties/default",
                                "type": "string",
                                "title": "The radio field default schema",
                                "description": "Specifies the default radio field option.",
                                "default": "",
                                "examples": [
                                    "standard"
                                ]
                            },
                            "options": {
                                "$id": "#/properties/fields/items/anyOf/5/properties/options",
                                "type": "object",
                                "title": "The radio field options schema",
                                "description": "An array of objects defining the radio button group options.",
                                "default": {},
                                "examples": [
                                    {
                                        "standard": "Standard",
                                        "hr": "Human Resources"
                                    }
                                ],
                                "properties": {
                                    "Standard": {
                                        "$id": "#/properties/fields/items/anyOf/5/properties/options/properties/Standard",
                                        "type": "string",
                                        "title": "The Standard schema",
                                        "description": "An explanation about the purpose of this instance.",
                                        "default": "",
                                        "examples": [
                                            "Standard"
                                        ]
                                    }
                                },
                                "additionalProperties": true
                            }
                        },
                        "additionalProperties": true
                    },
                    {
                        "$id": "#/properties/fields/items/anyOf/6",
                        "type": "object",
                        "title": "The cascade field schema",
                        "description": "Defines a cascading dropdown menu that dynamically updates based on the specified select field.",
                        "default": {},
                        "examples": [
                            {
                                "type": "cascade",
                                "name": "cascadeEx",
                                "label": "Cascade Example",
                                "dependsOn": "BUILD",
                                "options": {
                                    "standard": {
                                        "std1": "Standard 1",
                                        "std2": "Standard 2",
                                        "std3": "Standard 3"
                                    },
                                    "hr": {
                                        "hr1": "Human Resources 1",
                                        "hr2": "Human Resources 2",
                                        "hr3": "Human Resources 3"
                                    }
                                }
                            }
                        ],
                        "required": [
                            "type",
                            "name",
                            "label",
                            "dependsOn",
                            "options"
                        ],
                        "properties": {
                            "type": {
                                "$id": "#/properties/fields/items/anyOf/6/properties/type",
                                "type": "string",
                                "title": "The cascade field type schema",
                                "description": "The cascade field type.",
                                "default": "cascade",
                                "examples": [
                                    "cascade"
                                ]
                            },
                            "name": {
                                "$id": "#/properties/fields/items/anyOf/6/properties/name",
                                "type": "string",
                                "title": "The cascade field name schema",
                                "description": "The cascade field variable name.",
                                "default": "",
                                "examples": [
                                    "cascadeEx"
                                ]
                            },
                            "label": {
                                "$id": "#/properties/fields/items/anyOf/6/properties/label",
                                "type": "string",
                                "title": "The cascade field label schema",
                                "description": "The cascade field label.",
                                "default": "",
                                "examples": [
                                    "Cascade Example"
                                ]
                            },
                            "dependsOn": {
                                "$id": "#/properties/fields/items/anyOf/6/properties/dependsOn",
                                "type": "string",
                                "title": "The cascade field dependsOn schema",
                                "description": "Specifies the select field name to monitor.",
                                "default": "",
                                "examples": [
                                    "BUILD"
                                ]
                            },
                            "options": {
                                "$id": "#/properties/fields/items/anyOf/6/properties/options",
                                "type": "object",
                                "title": "The options schema",
                                "description": "An object containing an array of nested objects defining the dependant cascade menu items.",
                                "default": {},
                                "examples": [
                                    {
                                        "standard": {
                                            "std1": "Standard 1",
                                            "std2": "Standard 2",
                                            "std3": "Standard 3"
                                        },
                                        "hr": {
                                            "hr1": "Human Resources 1",
                                            "hr2": "Human Resources 2",
                                            "hr3": "Human Resources 3"
                                        }
                                    }
                                ],
                                "additionalProperties": true
                            }
                        },
                        "additionalProperties": true
                    }
                ]
            }
        }
    },
    "additionalProperties": true
}