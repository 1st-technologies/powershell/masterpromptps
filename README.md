# MasterPromptPS

MasterPromptPS is a Light Touch build menu generator for Microsoft Endpoint Configuration Manager. Values selected in the form are stored as task sequence variables.

[[_TOC_]]
  
![Sample Image](images/sample.png)

## Usage

`MasterPromptPS.ps1 [[-SettingsFile] <String>] [-HideProgress] [-SCCM] [-Test] [<CommonParameters>]`

## Parameters

### SettingsFile

Specifies the json file with the settings and menu definition.

* **Type**: String
* **Default Value**: `"$PSScriptRoot\MasterPromptPS.json"`

### HideProgress

Hide the SCCM/MDT task sequence progress bar.

* **Type**: SwitchParameter

### SetVariables

Generate SCCM/MDT Task Sequence variables.

* **Type**: SwitchParameter

### Test

Run in test mode.

* **Type**: SwitchParameter

#### CommonParameters

This cmdlet supports the common parameters: Verbose, Debug, ErrorAction, ErrorVariable, WarningAction, WarningVariable, OutBuffer, PipelineVariable, and OutVariable. For more information, see about_CommonParameters [https:/go.microsoft.com/fwlink/?LinkID=113216](https:/go.microsoft.com/fwlink/?LinkID=113216).

## Examples

`PS C:\>.\MasterPromptPS -SetVariables`

Display a prompt as defined by the MasterPromptPS.json file (default) located in the current directory and store results in answers.json and as task sequence variables.

`PS C:\>.\MasterPromptPS -HideProgress -SetVariables`

Hide the MDT or SCCM progress bar, display a prompt as defined by the MasterPromptPS.json file (default) located in the current directory and store results in answers.json and as task sequence variables.

`PS C:\>.\MasterPromptPS -SettingsFile .\server.json`

Display a prompt as defined by .\server.json and store results in answers.json.

## Settings File

Settings and menu definition are stored in a .json file. If a file named MasterPromptPS.json is found in the current directory, MasterPromptPS will use that.

MasterPromptPS supports the following field types:

* Informational labels using WMI as the data source
* Text fields with max length parameter and WMI data as source for default text
* Dropdown lists and cascade dropdown lists
* Masked password fields which store data as encrypted text.
  **NOTE**: Passwords can be only decrypted on the same system on which they were encrypted.
* Checkboxes and Radio buttons

### Global Settings

The settings file contains a number of global properties and settings:

* **width**: The width of the dialog box
* **height**: The height of the dialog box
* **rowHeight**: The height of each field row
* **labelColumnX**: The initial horizontal position of the label column in pixels
* **fieldColumnX**: The initial horizontal position of the field column in pixels
* **rowY**: The vertical position of the first field row in pixels
* **textBoxSize**: The length of text boxes, password fields, dropdown lists and radio button groups in pixels
* **title**: The window title
* **header**: Text to appear beside the logo in the header of the form
* **footer**: Text to appear in the footer of the form (not currently enabled)
* **errorColor**: Set the background of text, password and select field if they do not validate

### Field Types

#### Common Parameters

All field types require the following three parameters

* **type**: [info | text | password | select | cascade | check | radio]
* **name**: The name of the variable to store the resulting selection
  **NOTE**: select, cascade and radio also store the value of the label as %VAR%_label
* **label**: The label to display beside the field

#### info

* **query**: A WQL query as data source

#### text

* **required**: [ true | false] specifies if the field is mandatory
* **default**: a WQL query to provide a default value
* **maxLength**: the maximum allowed length of the field

#### password

* **required**: [ true | false] specifies if the field is mandatory

#### check

* **checked**: specifies if the field is checked by default

#### radio

* **default**: specifies the default selection
* **options**: a json object array of value/label pairs to define radio buttons

**NOTE**: Radio Buttons are packed right to left. Radio button options must be specified in reverse order in the JSON file.

#### select

* **options**: a json object array of value/label pairs to define dropdown menu options

    OR

* **default**: a wmi query that selects at least fields
* **wmiLabel**: the wmi property to assign to the dropdown menu label
* **wmiValue**: the wmi property to assign to the dropdown menu value

#### cascade

* **dependsOn**: The select field to base the selection options on
* **options**: an array of nested json object arrays with select values and sub options per dependent selection menu item

### Example Settings File

```JSON
{
    "__version__": "0.1.4",
    "__description__": "Sample settings file",
    "width": 500,
    "height": 585,
    "rowHeight": 25,
    "labelColumnX": 10,
    "fieldColumnX": 150,
    "rowY": 90,
    "textBoxSize": 320,
    "title": "Build Prompt",
    "header": "",
    "footer": "Warning: This system is about to be rebuilt resulting in the loss of all data. If you DO NOT wish to proceed, immediately power off and remove any thumb drives.",
    "errorColor": "#FFFF77",
    "headerColor": "#47154A",
    "fields": [{
            "type": "info",
            "name": "infIpAddress",
            "label": "IP Address",
            "query": "SELECT IPAddress FROM Win32_NetworkAdapterConfiguration WHERE IPEnabled = True AND DHCPEnabled = True"
        },
        {
            "type": "info",
            "name": "InfMacAddress",
            "label": "MAC Address",
            "query": "SELECT MACAddress FROM Win32_NetworkAdapterConfiguration WHERE IPEnabled = True AND DHCPEnabled = True"
        },
        {
            "type": "text",
            "name": "OSDComputerName",
            "label": "Computer Name",
            "default": "SELECT SerialNumber FROM Win32_BIOS",
            "required": true,
            "maxLength": 15
        },
        {
            "type": "select",
            "name": "BUILD",
            "label": "Build",
            "options": {
                "standard": "Standard",
                "marketing": "Marketing",
                "it": "IT"
            }
        },
        {
            "type": "select",
            "name": "nic",
            "label": "NIC",
            "default": "select Name,Index from Win32_NetworkAdapter where NetConnectionStatus = 2 AND AdapterTypeID = 0",
            "wmiLabel": "Name",
            "wmiValue": "Index"
        },
        {
            "type": "password",
            "name": "PASSWORD",
            "label": "Password",
            "required": true
        },
        {
            "type": "check",
            "name": "OPTION1",
            "label": "Option 1?",
            "checked": true
        },
        {
            "type": "radio",
            "name": "radioEx",
            "label": "Radio Example",
            "default": "standard",
            "options": {
                "standard": "Standard",
                "marketing": "Marketing",
                "it": "IT"
            
            }
        },
        {
            "type": "radio",
            "name": "radioEx2",
            "label": "Radio Example 2",
            "default": "rexO2",
            "options": {
                "rexO1": "Rex Opt 1",
                "rexO2": "Rex Opt 2",
                "rexO3": "Rex Opt 3"
            
            }
        },
        {
            "type": "cascade",
            "name": "cascadeEx",
            "label": "Cascade Example",
            "dependsOn": "BUILD",
            "options": {
                "standard": {
                    "std1": "Std 1",
                    "std2": "Std 2",
                    "std3": "Std 3"
                },
                "marketing": {
                    "m1": "M 1",
                    "m2": "M 2",
                    "m3": "M 3"
                },
                "it": {
                    "it1": "IT 1",
                    "it2": "IT 2",
                    "it3": "IT 3"
                }
            }
        }
    ]
}
```

## Validation

Text, password and select fields are validated for non empty, non null values by default.

## Answer File

This script will save all answers in the answers.json file in the current directory.

### Sample Answer File

```JSON
{
    "dnsSuffix":  "test.local",
    "adapter_label":  "Intel(R) Wi-Fi 6E AX211 160MHz",
    "gateway":  "1.1.1.1",
    "subnetMask":  "255.255.255.0",
    "dnsServers":  "1.1.1.3",
    "ipAddress":  "1.1.1.2",
    "adapter":  1
}
```

### Custom Validation

To add custom validation, add a PowerShell script to the customvalidators folder named the same as the Text or Password fields you wish to validate.
The script must return a boolean value indicating if the field is valid and display any corrective messages if required.

Custom validation scripts can also be used as a warning only (i.e. will not halt processing) if all exit paths are set to $True.

#### Example Script

This script will confirm if the OSDComputerName field begins with any of a set of allowed prefixes. If not it displays a popup message and returns boolean $False.

```PowerShell
Param(
    [string]$Value
)

$Allowed = @(
    "US",
    "UK",
    "ES"
)

$Buttons = [System.Windows.Forms.MessageBoxButtons]::OK
$Icon = [System.Windows.Forms.MessageBoxIcon]::Error# <-- Change to' Warning' to use as a 'warning' only prompt
$Message = "The computer name must begin with one of the following: $($Allowed -join ', ')"
$Title = "Error"


If(($Value[0..1] -join '').ToLower() -notin $Allowed){
    [System.Windows.Forms.MessageBox]::Show($Message,$Title,$Buttons,$Icon) | Out-Null
    $false # <-- Change to $true to use as a 'warning' only prompt
}else{
    $true
}
```

### Custom Actions

After the form is validated and submitted, this script will execute every .ps1 file in the customactions subdirectory.

#### Example Custom Action

```PowerShell
# NOTE: This script must be written to operate within WinPE which does not include thr NetTCPIP module.

# Get Answers
$answers = Get-Content -Path "$PSScriptRoot\..\answers.json" | ConvertFrom-Json

Write-Output "Configuring $($answers.adapter_label)..."

# Get the network adapter by index
$adapter = Get-CimInstance -ClassName Win32_NetworkAdapterConfiguration -Filter "Index = $($answers.adapter)"

# Set the IP address, subnet mask, and default gateway
$adapter | Invoke-CimMethod -MethodName EnableStatic -Arguments @{IPAddress=@($($answers.ipAddress)); SubnetMask=@($($answers.subnetMask))}
$adapter | Invoke-CimMethod -MethodName SetGateways -Arguments @{DefaultIPGateway=@($($answers.gateway))}

# Set the DNS servers
$adapter | Invoke-CimMethod -MethodName SetDNSServerSearchOrder -Arguments @{DNSServerSearchOrder=$($($answers.dnsServers) -split ",")}

# Set the domain
$adapter | Invoke-CimMethod -MethodName SetDNSDomain -Arguments @{DNSDomain=$($answers.dnsSuffix)}
```

### Example Command Lines

`PS C:\> MasterPromptPS.ps1 -SettingsFile sample_menu.json -Test`

Display a dialog box as defined in sample_menu.json, do not run custom actions and enable pressing ESC to quit. Validate the entries as defined in the JSON file and according to the boolean result of powershell scripts in the customactions directory. Save the results in "answers.json".

`PS C:\> MasterPromptPS.ps1 -SettingsFile sample_menu.json -HideProgress -SetVariables`

Display a dialog box as defined in sample_menu.json and run custom actions. Validate the entries as defined in the JSON file and according to the boolean result of powershell scripts in the customactions directory. Save the results in "answers.json", temporarily hide the SCCM/MDT progress dialog and save all values as Task Sequence variables.

## Known Issues

* ~~This script currently only supports 1 set of radio buttons per settings file.~~
* ~~This script will error if executed outside of an SCCM task sequence environment. The dialog can still be previewed~~ You can now use the -Test Parameter to test GUI creation when operating outside of a Task Sequence environment.

## To-Do

* ~~Add cascading select menus~~
* ~~Add custom validation~~
* ~~Add radio button groups~~
* ~~Add footer field~~
* Add datafile validation
