Param(
    [string]$Value
)

$Result=$True
$Buttons = [System.Windows.Forms.MessageBoxButtons]::OK
$Icon = [System.Windows.Forms.MessageBoxIcon]::Error
$Title = "Error"

function Test-ValidIp {
    param (
        [Parameter(Mandatory=$true)]
        [string]$ip
    )

    try {
        [System.Net.IPAddress]::Parse($ip)
        return $true
    }catch {
        return $false
    }
}

If(!([String]::IsNullOrEmpty($Value))){
    If((Test-ValidIp -ip $Value) -eq $false){
        [System.Windows.Forms.MessageBox]::Show("Invalid IP Address specified: $Value", $Title, $Buttons, $Icon) | Out-Null
        $Result = $false
    }
}else{
    [System.Windows.Forms.MessageBox]::Show("No IP Address specified", $Title, $Buttons, $Icon) | Out-Null
    $Result = $false
}

$Result