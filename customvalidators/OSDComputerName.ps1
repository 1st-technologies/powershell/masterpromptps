Param(
    [string]$Value
)

$Allowed = @(
    "US",
	"UK",
	"ES"
)

$BadChars = @(
	"\",
	"/",
	":",
	"*",
	"?",
	"!",
	" ",
	"'",
	'"',
	"<",
	">",
	"|",
	"_",
	".",
	",",
	"~",
	"@",
	"#",
	"$",
	"%",
	"^",
	"&",
	"(",
	")",
	"{",
	"}"
)


$CharResult=$true
$Result=$True
$Buttons = [System.Windows.Forms.MessageBoxButtons]::OK
$Icon = [System.Windows.Forms.MessageBoxIcon]::Error
$Title = "Error"


If(($Value[0..1] -join '').ToLower() -notin $Allowed){
    [System.Windows.Forms.MessageBox]::Show("The computer name must begin with one of the following: $($Allowed -join ', ')",$Title,$Buttons,$Icon) | Out-Null
    $Result=$false
}

# Test for bad chars
$Value.ToCharArray() | ForEach-Object{If($_ -in $BadChars){$CharResult=$false}}
If($CharResult -eq $false){
	[System.Windows.Forms.MessageBox]::Show("The computer name cannot contain any of the following: $($BadChars -join ', ')",$Title,$Buttons,$Icon) | Out-Null
    $Result=$false
}

$Result