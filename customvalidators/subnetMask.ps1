Param(
    [string]$Value
)

$Result=$True
$Buttons = [System.Windows.Forms.MessageBoxButtons]::OK
$Icon = [System.Windows.Forms.MessageBoxIcon]::Error
$Title = "Error"

function Test-ValidSubnetMask {
    param (
        [Parameter(Mandatory=$true)]
        [string]$subnetMask
    )

    $subnetBytes = $subnetMask.Split('.')
    if ($subnetBytes.Count -ne 4) {
        return $false
    }

    $binaryStr = ''
    foreach ($byte in $subnetBytes) {
        if ($byte -match '^\d+$' -and $byte -ge 0 -and $byte -le 255) {
            $binaryStr += [Convert]::ToString($byte, 2).PadLeft(8, '0')
        } else {
            return $false
        }
    }

    if ($binaryStr -match '^1*0*$') {
        return $true
    } else {
        return $false
    }
}

If(!([String]::IsNullOrEmpty($Value))){
    If((Test-ValidSubnetMask -subnetMask $Value) -eq $false){
        [System.Windows.Forms.MessageBox]::Show("Invalid Subnet Mask specified: $Value", $Title, $Buttons, $Icon) | Out-Null
        $Result = $false
    }
}else{
    [System.Windows.Forms.MessageBox]::Show("No Subnet Mask specified", $Title, $Buttons, $Icon) | Out-Null
    $Result = $false
}

$Result