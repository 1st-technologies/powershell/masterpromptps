Param(
    [string]$Value
)

$Result=$True
$Buttons = [System.Windows.Forms.MessageBoxButtons]::OK
$Icon = [System.Windows.Forms.MessageBoxIcon]::Error
$Title = "Error"

function Test-ValidIp {
    param (
        [Parameter(Mandatory=$true)]
        [string]$ip
    )

    try {
        [System.Net.IPAddress]::Parse($ip)
        return $true
    }catch {
        return $false
    }
}

If(!([String]::IsNullOrEmpty($Value))){
    foreach($dnsServer In $Value -split ","){
        If((Test-ValidIp -ip $dnsServer) -eq $false){
            [System.Windows.Forms.MessageBox]::Show("Invalid IP Address specified for DNS Server: $dnsServer", $Title, $Buttons, $Icon) | Out-Null
            $Result = $false
        }
    }
}else{
    [System.Windows.Forms.MessageBox]::Show("No DNS Servers specified", $Title, $Buttons, $Icon) | Out-Null
    $Result = $false
}

$Result